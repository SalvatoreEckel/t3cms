<?php
if (!defined ('TYPO3_MODE')) die ('Access denied.');

$GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'] .= ",t3themes_conf";

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
TCAdefaults.tt_content.sectionIndex = 0
TCEFORM.tt_content {
	layout {
		addItems {
			40 = Wrap with Container
			41 = Wrap with Fluid Container
		}
		altLabels {
			40 = Wrap with Container
			41 = Wrap with Fluid Container
		}
		#types.plugin.removeItems = 5
	}
}
');

# Enable flux
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('flux')) {
	\FluidTYPO3\Flux\Core::registerProviderExtensionKey('SalvatoreEckel.T3cms', 'Content');
}
