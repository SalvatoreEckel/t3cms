<?php

# Extension Manager/Repository config file for ext: "t3cms"

$EM_CONF[$_EXTKEY] = [
	'title' => 'TYPO3 Content Management System - Set up your homepages easier with it!',
	'description' => 'Works with all famous dists and sitepackages. Hardly inspired by high quality wordpress themes. EXT:t3cms is the core and toolbox for TYPO3 themes.',
	'category' => 'module',
	'author' => 'Salvatore Eckel',
	'author_email' => 'salvaracer@gmx.de',
	'author_company' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'clearCacheOnLoad' => 1,
	'version' => '3.2.0',
	'constraints' => [
		'depends' => [
			'typo3' => '8.7.0-9.5.99',
		],
		'conflicts' => [
		],
		'suggests' => [
			'flux' => '9.0.0-9.2.99',
		],
	],
];
